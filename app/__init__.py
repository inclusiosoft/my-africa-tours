# My Africa Tours initialization script
# import the Flask class from the flask module
import os
from flask import Flask, render_template, redirect, url_for, request
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, current_user
from flask_admin import Admin, BaseView, expose
from flask_admin.contrib.sqla import ModelView

#app init
app = Flask(__name__)
app.config.from_object('config')
app.config['MAINTENANCE'] = False
db = SQLAlchemy(app)

#error handling
#sentry = Sentry(app, dsn='YOUR_DSN_HERE') #<-- find dsn and uncomment line 9

#create admin
admin = Admin(app, name='My Africa Tours', template_mode='bootstrap3')
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'login'
login_manager.init_app(app)

import routes, models
