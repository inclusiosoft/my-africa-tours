var tour_info = {
  /*
    format is;

      'cost': a number (most likely Integer),
      'duration': a string,
      'highlights': [] - an array of strings,
      'itenerary': [{},{},{},...,{}] - an array of day objects

      day1 of qnep is at tour_info.qnep['itenerary'][0] and so on
      also note that the length of the itenerary array is also the duration in days.
      And the last item in the itenerary does not have an accommodation attribute.

  */
  qenp:{
    'cost':1500,
    'duration': '5 days',
    'highlights':["Launch trip experience on the Kazinga channel","Lake Katwe Salt mines", "Crater lakes",
    "Cultural exposure","Sceneic landscapes","Game drives", "Chimpanzee tracking", "Equator line photo moments"],
    'itenerary':[
        {
          'title':"PICK UP FROM ENTEBBE INTERNATIONAL AIR PORT.",
          'content':"You will be picked from the airport and led to a waiting car by the driver/guide, then traverse \
          through different suburbs towards the capital. Enjoy a slow drive of about 1 hour. You will then check in at the \
          hotel for meals, refreshments and an overnight stay.",
          'mealplan':"Dinner/Lunch.  It will depend onto the time of arrival at the airport and check in time at the hotel.",
          'accomodation':["Tourist Nest Hotel / La’ Villa suites."]
        },
        {
          'title':"TRANSFER TO QUEEN ELIZABETH NATIONAL PARK",
          'content':"You will be picked by the driver/guide at 8:00am from your hotel after an early morning breakfast.\
          This journey will take you to the western part of Uganda following Kampala-Mbarara highway, along the way, you \
          will have a stopover at the Equator line where the world is divided into two hemispheres namely; the North and \
          South poles, for photo moments and if lucky a water experiment shall be observed. A slow drive through Lake Mburo \
          National Park will be rewarding with sightings that the Park has got to offer. Expect to enjoy the scenic views \
          and different vegetation covers. You will have a stopover in Mbarara town for lunch, refreshment and stretching.\
          There after proceed QENP through Bushenyi towards Katungulu gate.",
          'mealplan':"Lunch and Dinner",
          'accomodation':["Park view safari lodge","Jacana Safari lodge","Kyambura safari lodge"]
        },
        {
          'title':"EXPLORING QUEEN ELIZABETH NATIONAL PARK",
          'content':"As early as 6:00am, you will wake up and prepare for a memorable chimpanzee tracking in the Kyambura \
          gorge. Briefing from the UWA range guide will do us good before setting out into the wild to track down these \
          primates closest to human. You will return to the hotel for a late breakfast as you prepare for a launch cruise \
          along the Kazinga channel that connects the two lakes Edward and George. Get closer to water animals, the \
          Elephants taking a bath at the channel banks, the Pelicans, the Hippos among others. Enjoy looking at them as \
          they splash the waters high in the space. You will then return to the hotel and have lunch. Relax as we wait to \
          visit Lake Katwe and the salt mines so as to add colour to our evening as we watch the back drop of the setting sun.",
          'mealplan':"Breakfast, Lunch and Dinner",
          'accomodation':["Park view safari lodge","Jacana Safari lodge","Kyambura safari lodge"]
        },
        {
          'title':"TRANSFER BACK TO KAMPALA",
          'content':"After an early morning breakfast, you will prepare for a game drive, ranging from wildlife and crater \
          lakes to the magnificence of the Rwenzori mountain ranges. QENP has got a variety of animals such as the Bush \
          Bucks, Oribis, Baboons, Elephants; Jackson’s Hartebeests among others which are resident to the Park. Expect to \
          view the sun rise above the Rwenzori ranges. The vegetation types like the savannah, forests, acacia and the montane.\
          You will then return to the hotel and prepare to checkout; you will lunch at a selected hotel/restaurant in Fort Portal \
          town. You will follow Fort Portal highway via Mubende to Kampala. The great scenic views of the western arm of the rift \
          valley will be attracting, the Baboons will be waiting and waving bye to you as you drive through Kibaale forest National \
          Park. Out of the forest, the tea estates of Mpanga are fascinating; this will add gratification to your trip. As you \
          approach Mityana, from a distance, you will have a glance of Lake Wamala, surrounded by a swamp.",
          'mealplan':"Dinner/Lunch",
          'accomodation':["Tourist Nest Hotel"]
        },
        {//day 5, Note that day 5 does not have the accommodation attribute
          'title':'TRANSFER TO ENTEBBE  INTERNATIONAL AIRPORT',
          'content':"Transfer to Entebbe International Airport. You will be picked from the hotel by our guide/driver to \
          drive you to the airport. Thereafter our guide will help you with the checking in. As you enjoy your flight \
          back home, always don’t forget to recommend My Africa Tours to others, we hope to see you again.",
          'mealplan':"Meals will depend on the time of your flight. Breakfast and lunch."
        }
      ]
},
  mfalls:{
    'cost':1540,
    'duration':'5 days',
    'highlights':["Launch trip experience on the mighty River Nile","Sceneic landscapes","Hiking","Cultural exposure","Game drives", "Budongo eco forest experience"],
    'itenerary':[
      {//changed this from day objects to an array of days. day1 is at position 0 and the array length = duration
        'title':"Pick up from entebbe international air port.",
        'content':"You will be picked from the airport and led to awaiting car by our experienced guide, then traverse\
         through different suburbs towards Kampala the capital. Enjoy a slow drive of about 1 hour. You will check into\
         the hotel for refreshments and relaxation.",
         'mealplan':"Dinner/Lunch. It will depend on the time of arival and check in.",
         'accomodation':["Tourist Nest Hotel/La' Villa suite."]
       },
       {
         'title':"Transfer to murchison falls national park",
         'content':"Following an early morning breakfast, you will eb picked by our driver, who will later brief you on \
          net program, following the travel plan; you then embark on your wildlife safari to Murchison Falls National Park.\
          This adventurous and rewarding journey will take you 3-4hrs through Kampala-Gulu highway via Masindi. Along the way\
          , expect to enjoy the scenic views and different vegetation covers that Uganda has got to offer.\
          The mouth watering roadside roasted chicken, different types of meats, drinks also roasted bananas commonly known as \"gonja\"\
          will be waiting; a stopover in Masindi town for lunch, refsreshment and stretching. We shall proceed to MFNP,\
          through Budongo Forest to Kichwambanyobo gate towards the Southern bank.",
          'mealplan':"Breakfast, Lunch and Dinner",
          'accomodation':["Murchison River lodge","Sambiya Safari lodge","Pakuba Safari lodge"]
       },
       {
         'title':"Exploring Murchison Falls National Park",
         'content':"As early as 6:00am, you will wake up and prepare for a game drive. You will be briefed by the UWA \
         range guide and then drive deep into the wild. Expect to encounter animals like bush bucks, baboons, Oribis, \
         giraffes and if lucky a leopard since they are residents to the park. The vegetation types of the park will \
         leave you not the same as this will a life time memory to you. Thereafter, return to the Park for breakfast as \
         you prepare for a launch cruise to the bottom of the falls. You will then hike to the top of the falls for \
         magnificent views of the world’s longest river Nile. You will also have photo moments at the top of the world’s \
         greatest water fall.",
         'mealplan':"Breakfast, Lunch and Dinner",
         'accomodation':["Murchison River lodge","Sambiya Safari lodge", "Pakuba safari lodge"]
       },
       {
         'title':"MORNING GAME DRIVE, TRANSFER BACK TO KAMPALA",
         'content':"As early as 6:00am, we shall have a light breakfast before we proceed for the morning game drive, \
         after you will return to the hotel for an English buffet breakfast there after you will check out of the hotel\
          and the park a well. You will have lunch in Masindi and there-after set-off for Kampala, along the journey, \
          you are free to exchange and also talk about the whole safari. Enjoy jour journey back home and always don’t \
          forget to recommend My Africa Tours to others, we hope to see you again.",
          'mealplan':"Breakfast, Lunch and Dinner.",
          'accomodation':["Tourist Nest Hotel/La' Villa Suites"]
       },
       {
         'title':"TRANSFER TO ENTEBBE INTERNATIONAL AIRPORT",
         'content':"Transfer to Entebbe International Airport. You will be picked from the hotel by our guide and \
            driven\/dropped to the airport. Thereafter our guide will help you with the check-in. As you enjoy your \
            flight back home, always don’t forget to recommend My Africa Tours to others, we hope to see you again. ",
         'mealplan':[]
       }
     ]
  },
  bqz:{
    'cost':7750,
    'duration':'10 days',
    'highlights':["Equator experience", "Scenic landscapes","Gorilla tracking","Cultural Exposure", "Game drives",
    "Boat cruise on Kazinga channel", "Zanzibar beach line experience" ],
    'itenerary':
    [
      {//day 1
        'title':"ARRIVAL IN UGANDA (Pick up from Entebbe International Airport.)",
        'content':" Upon arrival at Entebbe international airport, you will be met by our safari guide; you will \
            transfer to Kampala, Uganda’s capital and check-in a hotel booked for you.  In case you have had an early \
            flight, you will have a brief tour of Kampala.",
         'mealplan':"Dine at the Prestige Hotel apartments.",
         'accomodation':["Prestige Hotel apartments"]
      },{//day 2
        'title':"ARRIVAL IN UGANDA (Pick up from Entebbe International Airport.)",
        'content':"Following an early morning breakfast, you will set off for the awaited Gorilla tracking adventure, \
            the journey to Bwindi will take you approximately 9-10hrs with a stop at the Equator, to have a chance for you \
            to stand in two hemispheres at the same time and take as many photographs as you can. You will proceed with the \
            journey through the beautiful verdant rolling hills of this part of the country; you will have lunch in Mbarara\
            town at a selected restaurant/hotel and then proceed to Bwindi Impenetrable Forest National Park.",
         'mealplan':"Breakfast, Lunch and Dinner.",
         'accomodation':["Silver Back Lodge, Buhoma Lodge", "Gorilla Resort Camp, Bwindi", "Backpackers Lodge Bwindi",]
      },{//day 3
        'title':"GORILLA TRACKING AND CULTURAL EXPOSURE.",
        'content':"Following an early breakfast, you will have a briefing from the guide at the Park headquarters \
            before you begin the pursuit in the thick forest for the gentle Giant Gorillas. Renowned for its impressive \
            Mountain Gorillas, it’s unpredictable how long it will take you before finding them since it depends onto \
            their movement. Once tracked and found, the time spent with these giant Gorillas is rare, awesome, moving \
            and exciting, it will leave you with long lasting memories of a truly unique experience. Return to the hotel \
            for lunch and later in the evening go for a community visit to interact with the local people. You will \
            participate in captivating traditional performances presented by women’s groups or orphaned children groups.",
         'mealplan':"Breakfast, Lunch and Dinner.",
         'accomodation':["Silver Back Lodge, Buhoma Lodge", "Gorilla Resort Camp, Bwindi", "Backpackers Lodge Bwindi",]
      },{//day 4
        'title':"TRANSFER TO QUEEN ELIZABETH NATIONAL PARK.",
        'content':"Following an early breakfast, at 9:00am depart from Bwindi for Queen Elizabeth National Park through\
            Ishasha southern sector, renowned for the tree climbing lions.  Enjoy a game drive and if lucky, you will see \
            Lions among others. Being Uganda’s most visited and popular National Park that lies 435 kilometers south\
            west (6 hour drive.) You will later in the afternoon have a rest as you watch the setting sun beyond the \
            horizons and the Mountain Rwenzori ranges.  ",
         'mealplan':"Lunch and supper.",
         'accomodation':[ "Park view Safari Lodge", "Jacana Safari Lodge", "Mweya Safari Lodge" ]
      },{//day 5
        'title':"EXPLORE QUEEN ELIZABETH NATIONAL PARK.",
        'content':"At 6:15am, enjoy a light breakfast (tea/ coffee with a snack) and depart for an early morning game\
            drive. Animals like Lions, herds of Cape Buffalos, Elephants, Water Bucks, and Uganda Kobs; if lucky a \
            Leopard will be sighted. Return to the lodge by 10:30 am and have a full English buffet breakfast as you \
            enjoy the views of Kazinga channel.  Later relax or swim in the inviting pool. Lunch will be served followed \
            by an afternoon launch cruise. The cruise takes you along the Kazinga channel that connects the two lakes \
            George and Edward a home to Hippos and you will see a profusion of animals drinking on he water shores.\
            Expect to see lots of animals and many varieties of birds at a close range. The launch trip starts at 1500hrs\
            and ends at 1700hrs. Later in the evening return to the lodge for relaxation and dinner.",
         'mealplan':"Breakfast, lunch and supper.",
         'accomodation':[ "Park view Safari Lodge", "Jacana Safari Lodge", "Mweya Safari Lodge" ]
      },{//day 6
        'title':"TRANSFER BACK TO KAMPALA.",
        'content':"Following a late breakfast, you will setoff for a game drive as you check out of the Park through \
            Katunguru gate via Fort Portal back to Kampala. On your way to Kampala, you will enjoy great panoramic views of \
            tea plantations and Ruwenzori mountain ranges at a distance. You will have lunch at a selected hotel/restaurant \
            in Fort Portal town.",
         'mealplan':"Breakfast, Lunch and Supper.",
         'accomodation':["Gately Inn Entebbe","Prestige Hotel Apartments","Tourist Nest Hotel","Holiday Express Hotel"]
      },{//day 7
        'title':"EXPLORING ENTEBBE TOWN.",
        'content':"Following a late breakfast at the hotel, you will have a chance to explore the beauty of\
            Entebbe town, blessed with white sand beaches, hotels, quiet environment, UWEC, among others, you \
            will visit the Uganda reptiles village to get closer to nature, snakes like the Gaboon Vipers, \
            Forest Cobras, African Rock Python and other reptiles like the Nile Crocodile, Monitor Lizard and \
            the different types of species. There after you will relax at a selected beach in Entebbe as you get \
            ready for yet another fascinating place, the crown jewel of Indian Ocean.",
         'mealplan':"Breakfast, Lunch and Supper",
         'accomodation':["Gately Inn Entebbe, Prestige Hotel Apartments", "Tourist Nest Hotel", "Holiday Express Hotel"]
      },{//day 8
        'title':"TRANSFER TO ZANZIBAR.",
        'content':"Our guide will brief you about your next flight to Zanzibar the “Crown jewel of Indian ocean”, \
            filled with beautiful beaches and hospitable people. It stands out to be the most rewarding and resting \
            place for any traveller wishing to relax in Africa. Zanzibar is a Tanzanian archipelago off the coast of \
            east Africa, on this main island, there is a historic trade center with Swahili and Islamic influences. \
            Its winding lanes present minarets, carved door ways and 19th century landmarks such as house of wonders,\
            a former sultans palace and above all the wide beaches lined with hotels.",
         'mealplan':"Half board.",
         'accomodation':["Overnight stay at selected hotel in Zanzibar"]
      },{//day 9
        'title':"RELAXATION IN ZANZIBAR.",
        'content':"On this 9th day of our safari, you will be having a treat to the Islamic culture and the \
            hospitality of the people in Zanzibar. You will have a beach tour on the shores of the Indian Ocean a\
            cultural exposure of the people at the cost. Later in the day, you will either swim in the inviting \
            swimming pool at the equator or have a sun bath in the white sands at the beach. Don’t miss out onto \
            the marvelous games both in-door and out door.",
         'mealplan':"Breakfast, Lunch and supper.",
         'accomodation':["Overnight stay at selected hotel in Zanzibar"]
      },{//day 10 ~ has no accomodation attribute
        'title':"TRANSFER BACK TO USA.",
        'content':"This day marks the memorable and exciting wildlife adventure, gorilla tracking, and the resting at the beaches in Zanzibar, Africa as a continent has got a lot to offer and once you decide to travel across Africa, am sure you will be modest. As you prepare to get back home, forget not to tell people about Africa and am sure you have got a lot to write about. You will be dropped at the airport to catch up with your next flight back home. This marks the end of our tour to Africa. At MY AFRICA TOURS, we hope to see you once again on yet another adventure.",
         'mealplan':"",
      }]
  },
  kidepo:{
    'cost':1700,
    'duration':'6 days',
    'highlights':["Game drives","Scenic views","Hiking","Bird watching","Cultural exposure","A visit to the Kanangorok hot springs","Kidepo seasonal River experience"],
    'itenerary':
    [
      {//day 1
        'title':"PICK UP FROM ENTEBBE INTERNATIONAL AIRPORT.",
        'content':"Upon arrival at the airport, our guide will be at your reception, you will be led to awaiting car. \
            The journey from Entebbe to Kampala the capital will take you approximately 1hr to check-in the hotel for \
            refreshments and relaxation.",
         'mealplan':"Dinner/Lunch. It will depend on the time of arrival and check in.",
         'accomodation':["La' Villa siutes","Prestige Hotel Apartments"]
      },{//day 2
        'title':"TRANSFER TO KIDEPO VALLEY NATIONAL PARK.",
        'content':"Following an early morning breakfast, you will be picked by our driver who will later brief you\
            about the 8-9 hours trip to the extreme end of North-Eastern Uganda. Following Kampala-Gulu highway, you will\
            have several stopovers at, Migyeera town for stretching and Karuma falls for photo moments, a slow drive \
            through Karuma forest reserve where the Baboons alongside the road will be fascinating. A long stop-over for\
            lunches at a selected restaurant in either Gulu or Kitgum then proceed to KVNP.",
         'mealplan':"Breakfast, Lunch and dinner.",
         'accomodation':["Apoka Rest Camp"]
      },{//day 3
        'title':"EXPLORING KIDEPO VALLEY NATIONAL PARK.",
        'content':"As early as 6:00am, you will go for an early morning game drive and drive into the wild. \
            Expect to encounter animals like bush bucks, Butchellas Zebras, Oribis, Giraffes and if lucky a leopard since \
            they are residents to the park. The vegetation types of the park will leave you not the same as this will a \
            life time memory to you. Thereafter, return to the Park for breakfast as you prepare for a community visit in \
            the evening, you will get exposed to the different traditional dances and songs from the Karimojongs and the \
            way their societies are organized.",
         'mealplan':"Breakfast, Lunch and Dinner.",
         'accomodation':["Apoka Rest Camp"]
      },{//day 4
        'title':"MORNING GAME DRIVE, TRANSFER TO GULU TOWN.",
        'content':"As early as 6:00am, you will have a light breakfast before you proceed for a morning game drive to\
            the Northern sector where the great seasonal river, Ostriches, Cheetahs and also the Hot springs are found.\
            From this part of the park, it’s where the endemic Karamoja Apalis and the secretary bird are found. \
            The attracting palm trees lined along the banks of Kidepo River makes it standout as an oasis in the desert.\
            After you will return to the camp for lunch and there after check out of the park. You will have refreshment\
            and a stretch, term it a stopover in Kitgum and there-after set-off for Gulu town for an overnight stay.\
            To add gratification to your trip, you will have a treat to the night life in Gulu at a selected discotheque.",
         'mealplan':"Breakfast, Lunch and Dinner.",
         'accomodation':["Acholi Inn","Bomah Hotel","Acholi Pride"]
      },{//day 5
        'title':"TRANSFER FROM GULU TO KAMPALA.",
        'content':"Following a late breakfast at the hotel, you will drive around Gulu town, and later start your\
            journey of about 7hrs back to Kampala city. You will have a packed Lunch or served on the way at a selected\
            restaurant and then proceed to Kampala.",
         'mealplan':"Breakfast, Lunch and supper.",
         'accomodation':["La'Villa suites","Prestige Hotel Apartments"]
      },{//day 6
        'title':"TRANSFER TO ENTEBBE INTERNATIONAL AIRPORT.",
        'content':"Transfer to Entebbe International Airport. You will be picked from the hotel by our guide and \
            driven/dropped to the airport. Thereafter our guide will help you with the check-in. As you enjoy your flight\
            back home, always don’t forget to recommend My Africa Tours to others, we hope to see you again.",
         'mealplan':""
      }
    ]
  }
};


var inex_union = {
  'inclusions':[ "Transport - Tour vehicle, driver and fuel.", "Activities i.e. Game drive, launch cruise,\
  salt mines visit, cultural exposure and chimp tracking.",  "Access and all activity fees.",  "Guiding fees.",
  "Non premium drinks.",  "Bottled water.",   "Meals and accommodation."],
  'exclusions':["Gratitude and tipping","Alcoholic drinks","Air tickets/transfers","Travel insurance",
  "Curion shop purchase", "Phone bill and other personal expenditures", "Laundry", "Any other program outside the itenerary"]
};

var NOTE = "My Africa Tours reserves the right to change the travel plan and itenerary as necessary.";
