# Site photos
All pictures/photos in this folder (`app/static/img`) are either in;
	- `jpg`  (>85%)
	- `png`  (~14%) or
	- `jpeg` (~1%) (`mytlogo.jpeg`)
format unless otherwise specified.

My africa tour photos are those that begin with `myt_`. 
All my africa tour photos (except originals) belong to a tour and they fall in either one of two categories;

1. main - Measures `626 x 464` and are displayed prominently. Special tour and feature tour photos are main photos.
2. detail - Measures `724 x 448` and require a secondary click to get to.
3. thumb - Measures `108 x 75` and also require a secondary click to get to.

Every tour photo has `main`, `detail` and `thumb` images. 

## Special Tour Photos
Special tour photos measure `626 x 464` and are featured prominently on the home page.\
They include;

1. Wellness Tour, Uganda		(`myt_special_tour-01.jpg`) -> (`myt_special_detail-01.jpg`)
2. Honeymoon Tour, Uganda		(`myt_special_tour-02.jpg`) -> (`myt_special_detail-02.jpg`)
3. SpringBreak Tour, Tanzania	(`myt_special_tour-03.jpg`) -> (`myt_special_detail-03.jpg`)
4. Relaxation Tour, kenya		(`myt_special_tour-04.jpg`) -> (`myt_special_detail-04.jpg`)

## Feature Tour Photos
Feature tour photos measure also `626 x 464` and they are featured prominently on `/home` page. They include;

1. Bwindi Tour, Uganda		(`myt_bwindi_main.jpg`)		->   (`myt_bwindi_detail.jpg`)
2. Serengeti, Kenya			(`myt_serengeti_main.jpg`)	-> (`myt_serengeti_detail.jpg`)
3. Masai Mara, Kenya		(`myt_maasaimara_main.jpg`) -> (`myt_maasaimara_detail.jpg`)
4. Zanzibar Tour, Tanzania	(`myt_feature_tour-04.jpg`) -> (`myt_feature_detail-04.jpg`)

## Detail Photos

When a use clicks a tour, they are taken to a page with details for the tour. The tour's detail photos are displayed on this page (all tour images with the category detail).
Dimensions: 
1. `724 x 448` for the main image (category: `detail`). Example: `myt_feature_detail-01.jpg`
2. `108 x 75` for the thumbnail images (category: `thumb`). Example: `myt_feature_thumb-01.jpg`

## Original copies
Original photos are named `original-0n.abc`, where n is the nth photo and `abc` is a either `jpg` or `png`.
Copies of the orignals can be resized to create pictures for different tours.\
For example, special tour pictures are resized photos of original copies.

Note: **DO NOT** resize original photos, resize a copy of the original. \
Make a copy by simply clicking on desired original and copying, then paste. **DO NOT** resize the original.


## Resizing 

The following are the dimensions required on each page.
All dimensions are in pixels

##/home

	Large slider - `2400 x 807` (first image)
	Feature tours - `657 x 486`
	Special offers - `626 x 464`
	Hottest offers - `99 x 99`
	Featured tours - `626 x 464` 
	Featured footer tours - `238 x 173` (`footer`)

##/tours

	Results - `626 x 464` (same as the special tours on the home page.)
	These are categorized as `main`

##/about
	Our Team photos - `551 x 491`

##/portfolio

	Portfolio photos - `700 x 535`

##/blog
	
	Standard blog format - `666 x 320`

##/contact

	None

##footer - featured deals
	Featured tours - `238 x 173` (`footer`)

## Lastly

	Consider adding to the body of this text to make this process work for everyone, thanks :)
	As always, please email me at `leevoper@gmail.com` for questions, concerns, and comments.
	
	*Happy Coding*
	*Leevoper*

