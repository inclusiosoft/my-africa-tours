from datetime import datetime
from app import db, admin, ModelView

# author: Leevoper

class Tour(db.Model):
    __tablename__ = 'tour'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True)
    price = db.Column(db.Float, index=True)
    duration = db.Column(db.Integer, index=True)
    review = db.relationship('Review', backref='tour', lazy='dynamic')
    transport_mode = db.Column(db.String(10), index=True)
    category = db.Column(db.String(64), index=True)
    country = db.Column(db.String(128), index=True)
    short_description = db.Column(db.String(100), index=True)
    long_description = db.Column(db.String(1024), index=True)
    images = db.relationship('Image', backref='tour', lazy='dynamic')

    def __unicode__(self):
        return self.name
    def __str__(self):
        return self.name
    def __repr__(self):
        return '<Name: %r, Price: %r, Country: %r>' % ( self.name, self.price, self.country )

class Image(db.Model):
    __tablename__ = 'image'
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(50), index=True)
    filename = db.Column(db.String(255), index=True)
    width = db.Column(db.Integer, index=True)
    height = db.Column(db.Integer, index=True)
    tour_id = db.Column(db.Integer, db.ForeignKey('tour.id'))

    def __unicode__(self):
        return self.filename
    def __str__(self):
        return self.filename
    def __repr__(self):
        return '<filename: %r,  width = %r, height = %r>' % ( self.filename, self.width, self.height )

class Review(db.Model):
    __tablename__ = 'review'
    id = db.Column(db.Integer, primary_key=True)
    author = db.Column(db.String(50), index=True)   # name of the person responsible for this review
    author_country = db.Column(db.String(29), index=True)  # the reviewer's country
    author_image = db.Column(db.String(100), index=True)   # the filename of the reviewer's photo
    post = db.Column(db.String(500), index=True)    # reviewer's post
    rating = db.Column(db.Float, index=True)      # the reviwer's rating
    tour_id = db.Column(db.Integer, db.ForeignKey('tour.id'))

    def __unicode__(self):
        return self.author
    def __str__(self):
        return self.author
    def __repr__(self):
        return '<author: %r, review: %r, rating: %r>' % ( self.author, self.post, self.rating )

class Comment(db.Model):
    __tablename__ = 'comment'
    id = db.Column(db.Integer, primary_key = True)
    body = db.Column(db.String(500))
    author = db.Column(db.String(50), index = True)
    author_image = db.Column(db.String(1090), index = True)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))

    def __repr__(self):
        return '<Comment: %r> ' % (self.body)

class Tag(db.Model):
    __tablename__ = 'tag'
    id = db.Column(db.Integer, primary_key = True)
    content = db.Column(db.String(512), index = True)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))

    def __unicode__(self):
        return self.content
    def __str__(self):
        return self.content
    def __repr__(self):
        return '<tag: %r>'  % (self.content)

class Post(db.Model):
    __tablename__ = 'post'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50), index=True)
    preview = db.Column(db.String(512))
    body = db.Column(db.String(2024))
    author = db.Column(db.String(64),index=True)
    category = db.Column(db.String(20), index=True)
    tags = db.relationship('Tag', backref='post', lazy='dynamic')
    timestamp = db.Column(db.DateTime, index=True)
    comment = db.relationship('Comment', backref='post', lazy='dynamic')

    def __repr__(self):
        return '<%r>' % (self.id)

class Subscriber(db.Model):
    __tablename__ = 'subscriber'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(20), index=True)

    def __repr__(self):
        return '%r' % (self.email)

# a tour can have many reviews so it makes sense to bundle reviwer info in review object
# I see no benefit of seperating reviewer attributes from review thus far
# class Reviewer(db.Model):
#     __tablename__ = 'reviewer'
#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(50), index=True)     # the reviewer's name
#     country = db.Column(db.String(29), index=True)  # the reviewer's country
#     image = db.Column(db.String(100), index=True)   # the reviewer's image
#
#     def __unicode__(self):
#         return self.name
#     def __str__(self):
#         return self.name
#     def __repr__(self):
#         return '<name: %r, country: %r'> % ( self.name, self.country )

class SubscriberView(ModelView):
    def __init__(self, session, **kwargs):
        super(SubscriberView, self).__init__(Subscriber, session, **kwargs)

class TagView(ModelView):
    def __init__(self, session, **kwargs):
        super(TagView, self).__init__(Tag, session, **kwargs)

class CommentView(ModelView):
    def __init__(self, session, **kwargs):
        super(CommentView, self).__init__(Comment, session, **kwargs)

class PostView(ModelView):
    def __init__(self, session, **kwargs):
        super(PostView, self).__init__(Post, session, **kwargs)

class ReviewView(ModelView):
    def __init__(self, session, **kwargs):
        super(ReviewView, self).__init__(Review, session, **kwargs)

class ImageView(ModelView):
    def __init__(self, session, **kwargs):
        super(ImageView, self).__init__(Image, session, **kwargs)

class TourView(ModelView):
    def __init__(self, session, **kwargs):
        super(TourView, self).__init__(Tour, session, **kwargs)

admin.add_view(SubscriberView(db.session))
admin.add_view(TagView(db.session))
admin.add_view(CommentView(db.session))
admin.add_view(PostView(db.session))
admin.add_view(TourView(db.session))
admin.add_view(ImageView(db.session))
admin.add_view(ReviewView(db.session))
