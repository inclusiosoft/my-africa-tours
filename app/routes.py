import random, datetime
from flask_wtf import FlaskForm
from flask_sqlalchemy import SQLAlchemy
from flask import render_template, flash, redirect, session, url_for, request, g
from wtforms.validators import Required, DataRequired, Email, Length, Regexp, EqualTo
from wtforms import Field, Label, BooleanField, FloatField, SelectField, StringField, \
SubmitField, TextAreaField, validators, ValidationError, IntegerField
from app import app, db, models, login_manager, admin
from models import Tour, Image

# Classes
class Newsletter(FlaskForm):
    email = StringField(u'Email',validators=[validators.DataRequired("Email")])
    submit = SubmitField('submit')

class ContactForm(Newsletter):
    firstname = StringField(u'Firstname',validators=[Required()])
    lastname = StringField(u'lastname',validators=[Required()])
    website = StringField(u'Website', validators=[Required()])
    message = TextAreaField(u'Message',[validators.DataRequired('Message')])

    def __repr__(self):
        return '<firstname=%s, lastname=%s, email=%s, website=%s>' % (self.firstname,\
            self.lastname, self.email, self.website)

class TourSearchForm(FlaskForm):
    tour = StringField(u'Tour',validators=[Required()])
    budget = FloatField(u'Budget',[validators.DataRequired("Budget")])
    departure = StringField(u'Departure',[validators.DataRequired("Departure")])
    arrival = StringField(u'Return',[validators.DataRequired("Return")])
    rating = SelectField(u'Rating',choices=[('zero','0'),('one','1'),('two','2'),('three','3'),('four','4'),('five','5')])
    people = SelectField(u'People',choices=[('zero','0'),('one','1-2'),('two','3-5'),('three','6-8'),('four','9-11'),('five','12-15')])
    country = SelectField(u'Country', choices=[('zero',''),('one','Uganda'),('two','Kenya'),('three','Tanzania'),('four','Rwanda'),('five','Burundi')])
    duration1 = BooleanField(u'checkbox')
    duration2 = BooleanField(u'checkbox')
    duration3 = BooleanField(u'checkbox')
    duration4 = BooleanField(u'checkbox')
    tour_type1 = BooleanField(u'checkbox')
    tour_type2 = BooleanField(u'checkbox')
    tour_type3 = BooleanField(u'checkbox')
    country1 = BooleanField(u'checkbox')
    country2 = BooleanField(u'checkbox')
    country3 = BooleanField(u'checkbox')
    country4 = BooleanField(u'checkbox')
    country5 = BooleanField(u'checkbox')
    country6 = BooleanField(u'checkbox')
    submit = SubmitField(u'Submit')


# Global variables
# myt_tours = Tour.query.all()

# addSubscriber
# input parameters: string email
# return: none
def addSubscriber(email):
    # ensure that input is the correct type
    if type(email) is str:
        subscriber = models.Subscriber()
        subscriber.email = email
        db.session.add(subscriber)
        db.session.commit()
    else:
        raise TypeError("Inputs must both be strings.")

# Routes
@app.before_request
def check_for_maintenance():
    if app.config['MAINTENANCE'] and request.path != url_for('maintenance'):
        return redirect(url_for('maintenance'))



@app.route('/', methods=['GET','POST'])
@app.route('/home', methods=['GET','POST'])
def home():
    nl = Newsletter()
    title = 'My Africa Tours'
    iform = TourSearchForm()
    if nl.submit.data: # save str(nl.email.data) in a database of subscribers
        addSubscriber(nl.email.data)
    return render_template('index.html', tform=iform, myt=Tour, Images=Image, newsletter=nl, title=title)

@app.route('/services', methods=['GET', 'POST'])
def services():
    nl = Newsletter()
    title = 'MYT|Services'
    if nl.submit.data:
        addSubscriber(nl.email.data)
    return render_template('services.html',newsletter = nl,title = title)

@app.route('/tours', methods=['GET','POST'])
def tours():
    nl = Newsletter()
    # global myt_tours
    tour_search = TourSearchForm()
    title = 'M.A.T|Tours'
    if nl.submit.data:
        addSubscriber(nl.email.data)
    return render_template('tours.html', myt = Tour, Images=Image, tsf = tour_search, newsletter = nl, title = title)

@app.route('/about', methods=['GET','POST'])
def about():
    nl = Newsletter()
    title = 'M.A.T|About Us'
    if nl.submit.data:
        addSubscriber(nl.email.data)
    return render_template('about_us.html', myt = Tour, Images = Image, newsletter = nl, title = title)

@app.route('/portfolio', methods=['GET','POST'])
def portfolio():
    nl = Newsletter()
    title = 'M.A.T|Portfolio'
    if nl.submit.data:
        addSubscriber(nl.email.data)
    return render_template('portfolio.html', myt = Tour, Images = Image, newsletter = nl, title = title)

@app.route('/blog', methods=['GET','POST'])
def blog():
    title = 'M.A.T|Blog'
    nl = Newsletter()
    posts = models.Post.query.all()
    if nl.submit.data:
        addSubscriber(nl.email.data)
    return render_template('blog.html', posts = posts, myt = Tour, Images = Image, newsletter = nl, title = title)

@app.route('/contact', methods=['GET','POST'])
def contact():
    title = 'M.A.T|Contact Us'
    nl = Newsletter()
    cf = ContactForm()
    if nl.submit.data:
        addSubscriber(nl.email.data)
    if cf.submit.data:
        breakpoint
        print cf.firstname.data
        print cf.lastname.data
        print cf.__repr__
        return redirect(url_for('contacts'))
    return render_template('contacts.html', myt = Tour, Images=Image, contactform = cf, newsletter = nl, title = title)

@app.route('/tour_detail/<tour_name>', methods=['GET','POST'])
def tour_detail(tour_name):
    title = 'M.A.T|Tour detail'
    nl = Newsletter()
    search_form = TourSearchForm()
    selected_tour = Tour.query.filter_by(name=tour_name.__str__())[0]
    # breakpoint
    try:
        print selected_tour
        return render_template('tour_detail.html', tour=selected_tour, myt=Tour, tsf = search_form, Images = Image, newsletter=nl, title=title)
    except Exception, e:
        return(str(e))

@app.route('/blog_post/<int:id>', methods=['GET','POST'])
def blog_post(id):
    nl = Newsletter()
    cform = ContactForm() # comment form
    post = models.Post.query.get(id)
    title = 'M.A.T|' + str(post.title)
    if str(cform.firstname.data) and cform.submit.data:
        # confirm the values
        print cform.firstname.data
        print cform.email.data
        print cform.message.data
        # add the new comment
        new_comment = models.Comment()
        new_comment.author = cform.firstname.data
        new_comment.body = cform.message.data
        # new_comment.post_id = post
        db.session.add(new_comment)
        db.session.commit()
    elif nl.validate_on_submit():
        addSubscriber(str(nl.email.data))
    return render_template('blog_post.html', post = post, myt = Tour, cform = cform, Images = Image, newsletter = nl, title = title)

@app.route('/maintenance', methods=['GET'])
def maintenance():
    title = 'M.A.T|Maintenance'
    return render_template('maintenance.html', title=title)

# route errors
@app.errorhandler(404)
def page_not_found(error):
    nl = Newsletter()
    title = 'M.A.T|404 error'
    app.logger.error('Page not found: %s', (request.path))
    return render_template('404.html', myt = Tour, Images = Image, newsletter = nl, title = title), 404

@app.errorhandler(500)
def internal_server_error(error):
    nl = Newsletter()
    title = 'M.A.T|500 error'
    app.logger.error('Server Error: %s', (error))
    return render_template('505.html', myt = Tour, Images = Image, newsletter = nl, title = title), 500

@app.errorhandler(Exception)
def unhandled_exception(e):
    nl = Newsletter()
    title = 'M.A.T|500 error'
    app.logger.error('Unhandled Exception: %s',(e))
    return render_template('500.html', myt = Tour, Images = Image, newsletter = nl, title = title), 500
