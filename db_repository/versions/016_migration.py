from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
review = Table('review', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('author', String(length=50)),
    Column('author_country', String(length=29)),
    Column('author_image', String(length=100)),
    Column('post', String(length=500)),
    Column('rating', Integer),
    Column('tour_id', Integer),
)

tour = Table('tour', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('name', VARCHAR(length=64)),
    Column('price', FLOAT),
    Column('duration', INTEGER),
    Column('reviews', INTEGER),
    Column('transport_mode', VARCHAR(length=10)),
    Column('category', VARCHAR(length=64)),
    Column('country', VARCHAR(length=128)),
    Column('short_description', VARCHAR(length=100)),
    Column('long_description', VARCHAR(length=1024)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['review'].create()
    pre_meta.tables['tour'].columns['reviews'].drop()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['review'].drop()
    pre_meta.tables['tour'].columns['reviews'].create()
