from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
subscriber = Table('subscriber', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('email', String(length=20)),
)

post = Table('post', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('preview', String(length=255)),
    Column('body', String(length=1024)),
    Column('category', String(length=20)),
    Column('timestamp', DateTime),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['subscriber'].create()
    post_meta.tables['post'].columns['preview'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['subscriber'].drop()
    post_meta.tables['post'].columns['preview'].drop()
