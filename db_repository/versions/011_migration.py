from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
image = Table('image', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('url', VARCHAR(length=255)),
    Column('timestamp', DATETIME),
    Column('tour_id', INTEGER),
    Column('category', VARCHAR(length=50)),
)

image = Table('image', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('url', String(length=255)),
    Column('dim', String(length=10)),
    Column('category', String(length=50)),
    Column('tour_id', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['image'].columns['timestamp'].drop()
    post_meta.tables['image'].columns['dim'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['image'].columns['timestamp'].create()
    post_meta.tables['image'].columns['dim'].drop()
