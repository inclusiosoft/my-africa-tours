from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
review = Table('review', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('author', String(length=50)),
    Column('author_country', String(length=29)),
    Column('author_image', String(length=100)),
    Column('post', String(length=500)),
    Column('rating', Float),
    Column('tour_id', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['review'].columns['rating'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['review'].columns['rating'].drop()
