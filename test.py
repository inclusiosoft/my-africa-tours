#!mytenv/bin/python
import routes
import os
import unittest

from config import basedir
from app import app, db
from app.models import Post, Subscriber, Tag, Review, Comment, Image, Tour

class TestCase(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATANASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'test.db')
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def tour_image(self):
        expected = 'image.jpg'

    if __name__ == '__main__':
        unittest.main()
