# My Africa Tours Website #

## Code for the my africa tours website ##
* Quick summary
* Version 1.0
* Created `alternate` branch

### Summary of set up
 1. Configuration
 2. Virtualenv
 3. Installing requirements & dependencies
 3. Database configuration
 4. How to run tests (coming soon)
 5. Deployment Instructions
 6. TODO
 7. Contacting the developers/company

### Configuration
 - First make sure that the virtual environment where you're running pip and python is setup in a directory above your app (see structure.txt) on how to set up your project.
 - Activate your virtual environment for example; `source mytenv/bin/activate`
 - Make sure you have the python-2.7.12 in the runtime.txt file
 - Configure heroku to use gunicorn in the Procfile `web: gunicorn app:app --log-file=-`
 - run `pip freeze > requirements.txt` to update your python app requirements 
 - run `heroku buildpacks:set heroku/python` to set the buildpack to python
 - Add `.env` and `*.pyc` to `.gitignore` to prevent project artifacts from affecting build
 - Log into heroku using your email and password
 - Commit and push

### Virtualenv
- Make sure that the virtual environment is configured correctly, i.e. ensure that pip executable uses python from virtualenv
- Also make sure that python is executed from `virtualenv` if not then `pip freeze > requirements.txt` followed by `rm -rf` `virtualenv` and `virtualenv myvenv` activate the new `virtualenv` 

### Installing requirements & dependencies
- All dependecies can be install via the `pip install -r requirements` command.

### Database configuration
 - Run `./db_create.py` to create a new database. This will create a new `app.db` database and a `/db_repository` 
 - Run `./db_migrate.py` to migrate changes in your `models.py` to the database
 - If you get `values too many to add error` comment out the last line you modified or any changes you modified then run `./db_migrate.py`. If it runs with no problems then uncomment your additions/changes and run `./db_migrate.py`. You can uncomment all and run that command or do it step by step in case you suspect that a single addition/change causes the error.

### How to run tests
 - Comming soon

### Deployment instructions
* Tell heroku that we are building a python app
* `heroku config:set BUILDPACK_URL=https://github.com/heroku/heroku-buildpack-python`
* Ensure that there's a Procfile in your app's main directory (which is one level above your app)
* Make sure that your app runs locally by running `heroku local web` to test it.
* Fix any routing problems your encounter before deploying else your app won't work 
* Further deployment instructions can be found in your heroku dashboard under the deploy tab

### Logging
 - `heroku logs --tail` for most recent logs
 - `heroku logs > errors.txt` to examine the logs in an error text file in the app

### Contribution guidelines ###
* ** No one should contribute unless they have developer/read & write access rights**
* To be updated soon
* Writing tests (coming soon)
* Code review (we plan on using reviewable)
* Other guidelines

### Problems & Troubleshooting
* Since this app is setup correctly, I really have no need to do this but these are some of the things I did when troubleshooting and resolving problems with my app.
* Read heroku logs after deploying your app and fix any problems or warnings you find
* Sometimes syntax errors prevent your app from running, for example writing `web:gunicorn app:app --log-file=-` instead of `web: gunicorn app:app --log-file=-` will result in an application error and your app will not run. 
* run `heroku ps` to make sure that your app is actually running (it will tell you that a dyno is being used) if not see second bullet point above.
* Make sure you have a `requirements.txt` and `runtime.txt` file in your project
* Make sure that you are using the right buildpack, if not then update it as described above and also inside your app's settings on the heroku dashboard of your app online.
* If you still have problems check your logs again, and search on `bing` for solutions, there's probably someone who's run into the same problems you have.

## Possible application errors 
 - Client terminated the request early and the application was still reading from the incoming data
- The database server was overloaded and could not handle the query
- A filesystem is full
- A harddrive crashed
- A backend server overloaded
- A programming error in a library you are using 
- Network connection of the server to another system failed

### TODO/Suggestions and Recommendations
1. Create a database of subscribers so that we can send weekly newsletters to them.
2. Automate sending
3. Incoporate build tools (i.e. travisCLI etc) into repository
4. Utilize git webhooks to automate deployment after `git push heroku master`
5. ReactJS
6. Routing selected tours to the tourDetails page 
   A tour is a dictionary of the tour name, price, duration, reviews, transport mode, image location, tour type, country, short description and long description
route needed items as a list e.g. if we are interested only in name, price, duration, image location, tour type, country and long description, we'd have a list of 7 items routed to /tour_detail/<list>
then we would unpack them or better we would send that list to the render_template function (or url_for() routing function) to populate /tour_detail/<list> whatever list turns out to be
7. Allow users to sort tour results by Name, Price, Duration, Country, and Reviews on tours page.
8. Fix featured tours links -> `details` page
9. Fix footer featured  tour links 
10. Automate sending of `emails/newsletters` to users who signup.
11. Enable/implement live reviews feature on details page.
12. Find another way to populate blog post tags, preferably using javascript + flask/jinja2 syntax
13. Delete Tags column once item 12 is complete. 
14. Make 1 row of tags seperated by a single space or a comma and link that to the post
15. Access things by `id` rather than `index`. See for loops in all pages.
16. Simplify adding of content, perhaps a WYSIWYG editor for the blog(wix-style).
17. [ October 1, 2017 ] Replace content in right column of `tour_details` page with the search form i.e. `enquire.html` for every tour.
### Who do I talk to? ###
* For questions, comments and conerns please email: leevoper@gmail.com
* Or info@myafricatours.com
